const app = require('../../app.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const path = 'about'

LOG.debug('Starting test/controllers/about.spec.js.')

mocha.describe('API Tests - About Controller', function () {
  mocha.describe('GET ' + path + '/t1', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/' + path + '/t1')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
  mocha.describe('GET ' + path + '/t1/a', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/' + path + '/t1/a')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
  mocha.describe('GET ' + path + '/t1/b', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/' + path + '/t1/b')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
})
